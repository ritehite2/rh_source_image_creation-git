# Rite-Hite OS Image Creation #

Automated building of root filesystem, app partition, store partition, and boot partitions

These are created as folders, off which the contents can then be copied on to a properly formatted sd card

The Rite Hite Smart Door GUI applications are not accounted for with this build. These are built using different methods

## Setup ##

### Install git-lfs... ###

```
#!bash
sudo apt-get install curl; curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt-get install git-lfs; git install lfs
```

### Install gawk, texinfo, chrpath (chroot)... ###
```
#!bash

sudo apt-get install texinfo, gawk, chrpath 
```
