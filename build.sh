#!/bin/bash

# Runs each build script for each project

set -e

cd $(dirname $0)

echo ""
echo ""
echo "For portability and consistency the root filesystem must be built in the build vm!!!"
echo ""
echo ""
sleep 10

#Installed SDK must be used for building.
#Do not attempt any other way for now.
#Path for build vm
DEFAULT_TOOLCHAIN=$(readlink -f ~/ti-processor-sdk/linux-devkit/sysroots/x86_64-arago-linux/usr/bin)
#likely ok if this exists
if [ ! -e "$DEFAULT_TOOLCHAIN"/arm-linux-gnueabihf-gcc ]
then
    echo "Default toolchain path: "$DEFAULT_TOOLCHAIN""
    echo "is not correct, exiting"
    exit 1
fi
DEFAULT_SYSROOT=$(readlink -f ~/ti-processor-sdk/linux-devkit/sysroots/cortexa8hf-vfp-neon-linux-gnueabi)
#likely ok if this exists
if [ ! -d "$DEFAULT_SYSROOT" ]
then
    echo "Default sysroot path: "$DEFAULT_SYSROOT""
    echo "is not correct, exiting"
    exit 1
fi
DEFAULT_RH_KERNEL_BUILD_PATH=rh_kernel/build.sh
DEFAULT_RH_ROOTFS_BUILD_PATH=rh_rootfs_creation/setup/build.sh
DEFAULT_RH_UBOOT_BUILD_PATH=rh_u-boot/build.sh
DEFAULT_RH_TSLIB_BUILD_PATH=rh_tslib/build.sh
DEFAULT_RH_QT_BUILD_PATH=rh_qt-5.6/build.sh
DEFAULT_RH_PSPLASH_BUILD_PATH=rh_psplash/build.sh

echo "#### Building rootfs ####"
echo ""
if [ ! -e "$DEFAULT_RH_ROOTFS_BUILD_PATH" ]
then
	echo "Missing build script or project for rootfs, exiting"
	exit 1
fi
#The build for rootfs carries it's own toolchain
( exec "$DEFAULT_RH_ROOTFS_BUILD_PATH" )
echo ""
echo "Rootfs Complete"
echo ""

echo "#### Building u-boot ####"
echo ""
if [ ! -e "$DEFAULT_RH_UBOOT_BUILD_PATH" ]
then
    echo "Missing build script or project for uboot, exiting"
    exit 1
fi
( exec "$DEFAULT_RH_UBOOT_BUILD_PATH" "$DEFAULT_TOOLCHAIN" )
echo "U-boot Complete"
echo ""

echo "#### Building kernel ####"
echo ""
if [ ! -e "$DEFAULT_RH_KERNEL_BUILD_PATH" ]
then
    echo "Missing build script or project for kernel, exiting"
    exit 1
fi
( exec "$DEFAULT_RH_KERNEL_BUILD_PATH" -t "$DEFAULT_TOOLCHAIN" )
echo "Kernel Complete"
echo ""

echo "#### Building psplash ####"
echo ""
if [ ! -e "$DEFAULT_RH_PSPLASH_BUILD_PATH" ]
then
    echo "Missing build script or project for psplash, exiting"
    exit 1
fi
( exec "$DEFAULT_RH_PSPLASH_BUILD_PATH" "$DEFAULT_TOOLCHAIN" )
echo "Psplash Complete"
echo ""

echo "#### Building tslib ####"
echo ""
if [ ! -e "$DEFAULT_RH_TSLIB_BUILD_PATH" ]
then
    echo "Missing build script or project for rootfs, exiting"
    exit 1
fi
( exec "$DEFAULT_RH_TSLIB_BUILD_PATH" "$DEFAULT_TOOLCHAIN" )
echo "tslib Complete"
echo ""
echo "#### Building qt ####"
echo ""
if [ ! -e "$DEFAULT_RH_QT_BUILD_PATH" ]
then
    echo "Missing build script or project for rootfs, exiting"
    exit 1
fi
( exec "$DEFAULT_RH_QT_BUILD_PATH" "$DEFAULT_TOOLCHAIN" "$DEFAULT_SYSROOT" )
echo "Qt Complete"
echo ""

echo "All builds complete, run deploy.sh"
echo ""
exit 0



