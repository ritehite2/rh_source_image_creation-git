#!/bin/bash
#Script to copy rootfs build from deploy folder or format and SD to the required partitions

MEDIA="/dev/shm/$SUDO_USER"
build_folder_path='./deploy'

function Check() {
    echo
    echo "Would you like to create an image from your SD(1) or partition your SD(2)?"
    echo "!!!WARNING!!! - The contents of SD card will be deleted"
    read IMAGE

    if [ $IMAGE == "1" ]; then
	    Rebuild   #Function call
    elif [ $IMAGE == "2" ]; then
	    Partition #Function call
    else
	    echo "Not an option. Please retry."
    fi
}


function Partition() {
    MEDIA="/media/$SUDO_USER"

    ls -al /dev/sd*
    echo
    cat << EOF

This script will format the secified disk.

!!!THE SPECIFIED DISK WILL BE FORMATTED AND ALL DATA WILL BE LOST!!!

Enter the device to be formatted, see above for a list of possible devices
EOF

    read disk

    if [ ${disk} == "/dev/sda" ]; then
        echo
        echo Are you sure? This device is typically a system disk
        echo Re-Enter the device to be formatted
        read disk
    fi

    if [[ ${disk} == "/dev/mmcblk"*"p"* ]]; then
        echo
        echo Whole disk entries required, not partitions
        echo
    elif [[ ${disk} == "/dev/sd"* ]]; then
        regex='^[0-9]+$'
        if [[ ${disk: -1} =~ $regex ]] ; then
        echo
        echo Whole disk entries required, not partitions
        exit 0
        fi
    fi

    if [ -e ${disk} ]; then
        echo ${disk} exists!
    else
        echo ${disk} does not exist
        exit 0
    fi

    echo Unmounting Disks...
    umount ${disk}* > /dev/null 2>&1

    echo Creating partitions...
#spaces in operations are required
    fdisk ${disk} << EOF > /dev/null 2>&1
o
n
p
1

4095
t
e
a
1
n
p
2

+350M
n
p
3

+850M
n
p

+125M
w
EOF

    echo Formatting  partitions...
    mkfs.vfat ${disk}1 > /dev/null 2>&1
    mkfs.ext4 -F ${disk}2 > /dev/null 2>&1
    mkfs.ext4 -F ${disk}3 > /dev/null 2>&1
    mkfs.ext4 -F ${disk}4 > /dev/null 2>&1

    echo Renaming partitions...
    fatlabel ${disk}1 boot > /dev/null 2>&1
    e2label ${disk}2 rootfs > /dev/null 2>&1
    e2label ${disk}3 app > /dev/null 2>&1
    e2label ${disk}4 store > /dev/null 2>&1

    sync
}

function Rebuild(){
    ls -al /dev/sd*
    echo
    cat << EOF
This script will delete the previous image and copy the image from the deploy folder.

!!!App partition is not modified, GUI applications are built using other tools!!!

Enter the device to be copied, see above for a list of possible devices
EOF

read disk

if [ ${disk} == "/dev/sda" ]; then
    echo
    echo Are you sure? This device is typically a system disk
    echo Re-Enter the device to be formatted
    read disk
fi

if [[ ${disk} == "/dev/mmcblk"*"p"* ]]; then
    echo
    echo Whole disk entries required, not partitions
    echo
elif [[ ${disk} == "/dev/sd"* ]]; then
    regex='^[0-9]+$'
    if [[ ${disk: -1} =~ $regex ]] ; then
	echo
	echo Whole disk entries required, not partitions
	exit 0
    fi
fi

if [ -e ${disk} ]; then
    echo ${disk} exists!
else
    echo ${disk} does not exist
    exit 0
fi
        
    echo "Making sure partions are currently unmounted..."
    umount "$disk"* > /dev/null 2>&1
    
    echo Mounting partitions to harmless location...
    mkdir -p $MEDIA/boot > /dev/null 2>&1
    mount ${disk}1 $MEDIA/boot > /dev/null 2>&1
    
    mkdir -p $MEDIA/rootfs > /dev/null 2>&1
    mount ${disk}2 $MEDIA/rootfs > /dev/null 2>&1
    
    mkdir -p $MEDIA/store > /dev/null 2>&1
    mount ${disk}4 $MEDIA/store > /dev/null 2>&1

    echo "Cleaning Rootfs..."
    find "$MEDIA"/rootfs -mindepth 1 -delete
    error
    
    echo "Cleaning Store..."
    find "$MEDIA"/store -mindepth 1 -delete
    error
        
    echo "Cleaning Boot..."
    find "$MEDIA"/boot -mindepth 1 -delete
    error
        
    echo "Rebuilding Rootfs..."
    cp -r $build_folder_path/rootfs/* $MEDIA/rootfs/ > /dev/null 2>&1
    error2
    
    echo "Rebuilding Store..."
    cp -r $build_folder_path/store/* $MEDIA/store/ > /dev/null 2>&1
    error2

    echo "Rebuilding Boot..."
    cp -r $build_folder_path/boot/* $MEDIA/boot/ 
    #> /dev/null 2>&1
    error2
    
    echo Unmounting partitions...
    sync
    umount $MEDIA/* > /dev/null 2>&1
    rm -r $MEDIA/* > /dev/null 2>&1

    echo "Done Rebuilding"
}

function error() {
    if [ $? -eq 0 ]; then
        echo "Cleaned Correctly"
    else
        echo "ERROR CLEANING"
        exit 
    fi
}

function error2() {
    if [ $? -eq 0 ]; then
        echo "Copied Correctly"
    else
        echo "ERROR COPYING"
        exit 
    fi
}

#Start of Script
if [[ $EUID > 0 ]]; then 
    echo "Please run as root/sudo"
    exit 1
else
    Check #Function call
fi
