#!/bin/bash

# Runs each deploy script for each project
# Handles filesystem modifications that do not have any other good place to be stored

set -e

cd $(dirname $0)

echo ""
echo "For portability and consistency the root filesystem must be built in the build vm!!!"
echo ""
sleep 5

DEFAULT_RH_ROOTFS_DEPLOY_PATH=rh_rootfs_creation/setup/deploy.sh
DEFAULT_RH_TSLIB_DEPLOY_PATH=rh_tslib/deploy.sh
DEFAULT_RH_QT_DEPLOY_PATH=rh_qt-5.6/deploy.sh
DEFAULT_RH_PSPLASH_DEPLOY_PATH=rh_psplash/deploy.sh
#deploy locations
DEFAULT_DEPLOY_FOLDER=deploy
DEFAULT_BOOT_DEPLOY="$DEFAULT_DEPLOY_FOLDER"/boot
DEFAULT_ROOTFS_DEPLOY_FOLDER="$DEFAULT_DEPLOY_FOLDER"/rootfs
DEFAULT_APP_DEPLOY_FOLDER="$DEFAULT_DEPLOY_FOLDER"/app
DEFAULT_STORE_DEPLOY_FOLDER="$DEFAULT_DEPLOY_FOLDER"/store

if [ -e "$DEFAULT_DEPLOY_FOLDER" ]
then
     #don't overwrite and existing deployment
     echo "A deploy appears to have been run, exiting"
     exit 1
fi

echo ""
echo "#### Creating deployment folders ####"
mkdir "$DEFAULT_DEPLOY_FOLDER"        # base directory
mkdir "$DEFAULT_BOOT_DEPLOY"          # u-boot
mkdir "$DEFAULT_ROOTFS_DEPLOY_FOLDER" # rootfs for kernel, tslib, psplash, and others
mkdir "$DEFAULT_APP_DEPLOY_FOLDER"    # app partition for qt
mkdir "$DEFAULT_STORE_DEPLOY_FOLDER"  # store partition for tslib settings

echo ""
echo "#### Deploying rootfs ####"
echo ""
if [ ! -e "$DEFAULT_RH_ROOTFS_DEPLOY_PATH" ]
then
	echo "Missing deploy script or project for rootfs, exiting"
 	exit 1
fi
( exec "$DEFAULT_RH_ROOTFS_DEPLOY_PATH" )

echo "#### Installing rootfs additions ####"
echo ""
#These items are tweaks made by us to the rootfs
#links for fonts now included with software package
mkdir "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/usr/share/fonts
ln -s /mnt/app/current/opt/app/resources/fonts "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/usr/share/fonts/rh_fonts
#symlink for store library
ln -s ../../opt/lib/librhstore.so.0 "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/usr/lib/librhstore.so.0
#move wpa_supplicant.conf to the right location
cp rootfs_additions/wifi/wpa_supplicant.conf "$DEFAULT_STORE_DEPLOY_FOLDER"
#firmware for approved usb wifi module
cp -r rootfs_additions/wifi/ath9k_htc "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/lib/firmware
#sets up mount points
cp rootfs_additions/fstab "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc
#remove screen terminal
cp rootfs_additions/inittab "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc
#disable automounting of sd card partitions
cp rootfs_additions/local.rules "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/udev/rules.d/
#remove banner
rm "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/rcS.d/S02banner.sh
#disable network script
rm "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/rc5.d/S01networking
#link to GUI startup
ln -s ../../opt/startup/rc.local "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/rc5.d/S97ritehite
#flip over for read only
sed -i '/ROOTFS_READ_ONLY/ s/=no/=yes/' "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/default/rcS
#NEED TO RECAPTURE
#copy library cache over
#cp rootfs_additions/ld.so.cache "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc
#rename the script to execute after launch of gui
#mv "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/rc5.d/S10dropbear "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/rc5.d/S99dropbear
#remove link to stop dropber launching for release
rm "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/rc5.d/S10dropbear
#enable key generation on store partition
cp rootfs_additions/dropbear "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/init.d/
mkdir "$DEFAULT_STORE_DEPLOY_FOLDER"/dropbear
#link resolv.conf to somewhere writeable
ln -s /var/run/resolv.conf "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/resolv.conf
#grab the required udhcpc.d script
hg clone https://bitbucket.org/rhdengineering/rh_network
cp rh_network/udhcpc_script/udhcpc.d/50default "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/udhcpc.d/
rm -r rh_network
#create the store mount point
mkdir "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/store
#create link for tmp
rm -r "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/tmp
ln -s -f /var/tmp "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/tmp
#update font cach location to rw location
cp rootfs_additions/50-user.conf "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/usr/share/fontconfig/conf.avail/
#create location for usb update to mount drives
mkdir "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/mnt/usb
#remove the rtc updates since these are uneccessary
rm "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/rc0.d/*hwclock.sh
rm "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/rc1.d/*hwclock.sh
rm "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/rc2.d/*hwclock.sh
rm "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/rc3.d/*hwclock.sh
rm "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/rc4.d/*hwclock.sh
rm "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/rc5.d/*hwclock.sh
rm "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/rc6.d/*hwclock.sh
#use this bootmisc.sh so as not to have the misleading change of RTC to timestamp in the event of failure
cp rootfs_additions/bootmisc.sh "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/init.d/
#Read only file system, links were created above to avoid errors these entries create
sed -i '/\/etc\/resolv\.conf/d' "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/default/volatiles/00_core
sed -i '\/tmp \/var\/tmp/d' "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/default/volatiles/00_core
#Uneccessary steps done by image on first boot
rm -r "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/ipk-postinsts
rm -r "$DEFAULT_ROOTFS_DEPLOY_FOLDER"/etc/rcS.d/S99run-postinsts

echo ""
echo "Rootfs additions - COMPLETE"
echo ""

echo "#### Deploying psplash ####"
echo ""
if [ ! -e "$DEFAULT_RH_PSPLASH_DEPLOY_PATH" ]
then
    echo "Missing deploy script or project for psplash, exiting"
    exit 1
fi
( exec "$DEFAULT_RH_PSPLASH_DEPLOY_PATH" )

echo "#### Deploying tslib ####"
echo ""
if [ ! -e "$DEFAULT_RH_TSLIB_DEPLOY_PATH" ]
then
    echo "Missing deploy script or project for rootfs, exiting"
    exit 1
fi
( exec "$DEFAULT_RH_TSLIB_DEPLOY_PATH" )

echo "#### Deploying qt ####"
echo ""
if [ ! -e "$DEFAULT_RH_QT_DEPLOY_PATH" ]
then
    echo "Missing deploy script or project for rootfs, exiting"
    exit 1
fi
( exec "$DEFAULT_RH_QT_DEPLOY_PATH" )

echo "All deploys complete"
exit 0
