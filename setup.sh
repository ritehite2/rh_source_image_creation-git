#!/bin/bash

#Script to download all sources for build image from scratch
#need to give the vm more memory, can't pull down through the webserver
#must use cifs for now
DEFAULT_SETUP_PATH=/mnt/windowsshares/departments/Engineering/Repos/git/projects/ritehite/rootfs_build/rh_rootfs_creation.git
DEFAULT_ROOTFS_SETUP_PATH=rh_rootfs_creation/setup/setup.sh

set -e

cd $(dirname $0)

echo ""
echo ""
echo "For portability and consistency the root filesystem must be built in the build vm!!!"
echo ""
echo ""
sleep 10

#pull down the rootfilesystem
if [ ! -e "$DEFAULT_SETUP_PATH" ]
then
    echo "setup for the roofs build is inaccessible: "$DEFAULT_SETUP_PATH""
    echo "change the path in this script to use a cifs path to the vcs server"
fi

echo "#### Cloning root filesytem project ####"
echo ""
git clone "$DEFAULT_SETUP_PATH"
echo "Running setup for root file system creation"
( exec $DEFAULT_ROOTFS_SETUP_PATH )
echo ""

#pull down u-boot
echo "#### Cloning u-boot project ####"
echo ""
hg clone https://vcs/hg/projects/ritehite/rh_u-boot/
echo ""

#pull down the kernel
echo "#### Cloning kernel project ####"
echo ""
hg clone https://vcs/hg/projects/ritehite/rh_kernel/
echo ""

#pull down psplash
echo "#### Cloning splash screen project ####"
echo ""
hg clone https://vcs/hg/projects/ritehite/rh_psplash/
echo ""

#pull down tslib
echo "#### Cloning tslib project ####"
echo ""
hg clone https://vcs/hg/projects/ritehite/rh_tslib/
echo ""

#pull down qt
echo "#### Cloning qt project ####"
echo ""
hg clone https://vcs/hg/projects/ritehite/rh_qt-5.6/
echo ""

echo "Complete, run build.sh next"
exit 0;
